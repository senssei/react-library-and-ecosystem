import React from 'react';
import { render } from 'react-dom';
import Hello from './Hello';

it('renders without crashing', () => {
  const div = document.createElement('div');
  render(<Hello name="CodeSandbox" />, div);
});
