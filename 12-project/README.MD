# Weather App

Your task is to prepare app according to spec below.

## Spec

* [Must] display a 5-day weather forecast
* [Should] display each day high/low temperatures
* [Should] have weather indication: sunny/cloudy/rainy/snowy
* [Could] have ability to hourly forecast after click on each day
* [Could] have ability to navigate to detailed hourly forecast by URL
* [Could] utilize real API like https://openweathermap.org/
* [Could] show temperature per hour as a graph

## Quality of service

* [Must] utilize ES2015+
* [Should] have tests (component / service)
* [Could] use store management
* [Could] use async helpers


## Known issues
