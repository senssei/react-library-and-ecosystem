
## Basics

Lunch break 1h  + 4x15 min (2h)

Slides, demo, project

1. React Basics - 1h 
    * React as ecosystem
    * React vs ReactDOM
    * Components vs PureComponents
    * Props vs State
2. CRA - 2h 
    * Toolset - IDE, plugins,
    * Additional tools and script - typescript, eml *
    * Styleguildes - review,
    * Ejecting - webpack *
3. React in depth - 3h
    * Lifecycle
    * Communication between components - Lift up / List up
    * PropTypes *
    * Lists (ids)
    * Virtual DOM
    * Ref - getElementById
    * Flow architecture
    * Controlled/Uncontrolled forms
    * Routing
    * REST - fetch API, axios

Lunch break 1h  + 4x15 min (2h)

## Advanced techniques

1. Higher order components (children) 1h *
    * functional programming
    * reactive programming
    * children
    * multi content extrapolation
2. Styled components 1h 
    * scope
    * styles as code
3. Redux 2h 
    * Redux architecture
    * Redux form
    * async Thunk vs Saga *
    * Redux Router
5. Quality Assurance 1h *
    * Jest
    * Flow
    * Enzyme
6. Deployment 30 min **
    * ENV
    * embeded sources
    * CORS
    * server-side rendering
7. Alternatives and cool things 30 min ***
    * Preact
    * Vue
    * Angular + React +? Redux
    * MobX
    * Zeit.js
    * GraphQL