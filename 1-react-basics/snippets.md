# Clean setup

https://codesandbox.io/s/l704jmlryz

# Simplest react component

http://babeljs.io/repl/#?babili=false&browsers=&build=&builtIns=false&code_lz=MYewdgzgLgBKC2AHGBeGAKAlKgfDAPACYCWAbngBICmANjSDAOogBONhAhAQPQnkDcAKEFA&debug=false&forceAllTransforms=false&shippedProposals=false&circleciRepo=&evaluate=true&fileSize=false&lineWrap=false&presets=latest%2Creact%2Cstage-2&prettier=false&targets=&version=6.26.0&envVersion=

# Almost whole exmple

http://babeljs.io/repl/#?babili=false&browsers=&build=&builtIns=false&code_lz=JYWwDg9gTgLgBAJQKYEMDGMAiB5AsnAMyghDgHIpUMBaAExLIG4AoZtCAOwGd53w4AvHAAUASkEA-OAB5awAG5SAEkgA2qiHADq0VbQCEMgPRzFLZsnRY8AOkodaSKMOl8wcIxIA0cemgCuIEgcMDYA5kgwAKKqSEEhAEIAngCStMIUEBAwZKKiLEA&debug=false&circleciRepo=&evaluate=true&lineWrap=false&presets=latest%2Creact%2Cstage-2&prettier=false&targets=&version=6.26.0

## Common typos

lowercase component, missing react, missing node

## Working example

```
import ReactDOM from 'react-dom';
import * as React from 'react';

const Comp = () => <div> Hello World! </div>;

ReactDOM.render(<Comp />, document.getElementById('root'));
```

## Classic Component vs Functional Component

```
import ReactDOM from 'react-dom';
import * as React from 'react';

const Comp = () => <div> Hello World! </div>;

class Comp2 extends React.Component {
  render() {
    return <h1> Hello World 2! </h1>;
  }
}

// ReactDOM.render(<Comp />, document.getElementById('root'));
ReactDOM.render(<Comp2 />, document.getElementById('root'));

```
## Props 
```
import ReactDOM from 'react-dom';
import * as React from 'react';

const Comp = props => <div> Hello World! {props.value} </div>;

class Comp2 extends React.Component {
  render() {
    // this.props.value += 2;
    return <h1> Hello World 2! {+this.props.value + 1} </h1>;
  }
}

// ReactDOM.render(<Comp value="2" />, document.getElementById('root'));
ReactDOM.render(<Comp2 value="2" />, document.getElementById('root'));

```
