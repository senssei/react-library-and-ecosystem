import React from "react"
import { render } from "react-dom"

import AddTodo from "./AddTodo"

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      todos: []
    }
  }

  addTodoToState = (newTodo) => {
   
    this.setState({
      todos: this.state.todos.concat(newTodo)
    })
  }

  render() {
    return (
      <section>
        <AddTodo addTodoToState={this.addTodoToState} />
      <div>
        {this.state.todos.map((todo, index) => {
          return <div key={index}>
              {todo.text} <br />
              {todo.desc}

          </div>
        })}
      </div>
     </section> 
    )
  }
}

render(<App />, document.getElementById("root"))