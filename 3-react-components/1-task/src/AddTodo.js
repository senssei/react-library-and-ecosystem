import React from "react"

class AddTodo extends React.Component {
  constructor() {
    super();
    this.state = {
      text: '',
      desc: ''
    }
  }

  changeText = (evt) => {
    this.setState({
      text: evt.target.value
    });
  }

  changeDesc = (evt) => {
    this.setState({
      desc: evt.target.value
    })
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.addTodoToState({
      text: this.state.text,
      desc: this.state.desc
    });


    this.setState({
      text: "",
      desc: ""
    })
  }

  render() {
    return (
      <section>
        <form onSubmit={this.onSubmit}>
          <input value={this.state.text} onChange={this.changeText} type="text" placeholder="Enter Text" />
          <input value={this.state.desc} onChange={this.changeDesc} type="text" placeholder="Enter Desc" />
          <button type="submit">Add Todo</button>
        </form>
      </section>
    )
  }
}

export default AddTodo;