import React from 'react';

export default ({ userName, onSearch, getRepos }) => (
  <div className="panel-block">
    <p className="control has-icons-left">
      <input
        className="input is-small"
        type="text"
        placeholder="username"
        value={userName}
        onChange={event => onSearch(event.target.value)}
      />
      <button onClick={() => getRepos()} className="button">
        Search
      </button>
    </p>
  </div>
);
