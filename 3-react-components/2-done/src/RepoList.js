import React from 'react';
import Repo from './Repo';

export default ({ repos }) => {
  const reposRows = repos.map(repo => <Repo key={repo.id} repo={repo} />);

  return (
    <table className="table is-striped is-fullwidth">
      <thead>
        <tr>
          <th>name</th>
          <th>description</th>
          <th>size</th>
          <th>forks</th>
          <th>starts</th>
        </tr>
      </thead>
      <tbody>{reposRows || null}</tbody>
    </table>
  );
};
