import React from 'react';
import { render } from 'react-dom';
import 'bulma/css/bulma.css';
import GitHub from 'github-api';

import SearchBar from './SearchBar';
import RepoList from './RepoList';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: 'senssei',
      repos: [],
      error: null
    };
  }

  componentWillMount() {
    this.getRepos(this.state.userName);
  }

  getRepos() {
    // unauthenticated client
    const gh = new GitHub();
    const user = gh.getUser(this.state.userName);
    user.listRepos().then(res => this.setState({ repos: res.data }));
  }

  render() {
    return (
      <nav className="panel">
        <SearchBar
          userName={this.state.userName}
          onSearch={name => this.setState({ userName: name })}
          getRepos={() => this.getRepos()}
        />
        <section>
          <p className="panel-heading">repositories</p>
          <RepoList repos={this.state.repos} />
        </section>
      </nav>
    );
  }
}
render(<App />, document.getElementById('root'));
