import React from 'react';

export default ({ repo }) => (
  <tr>
    <td>{repo.name}</td>
    <td>{repo.description}</td>
    <td>{repo.size}</td>
    <td>{repo.forks_count}</td>
    <td>{repo.stargazers_count}</td>
  </tr>
);
