import React from "react";
import { render } from "react-dom";
import Hello from "./Hello";

const App = () => <Hello user={{
  firstName: "Ben",
  lastName: "Harper"
}} />;

render(<App />, document.getElementById("root"));
