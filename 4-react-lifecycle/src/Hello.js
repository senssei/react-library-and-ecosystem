import React, {Component} from "react";

export default class Hello extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      greeting: 'Hello'
    }
    this.changeState = this.changeState.bind(this);
  }

  changeState () {
    this.setState({greeting: 'Goodbye'});
  }

  componentWillMount() {
    console.log('Hello: componentWillMount');
  }

  render() {
    console.log('Hello: render');
    return (
      <div>
        <h1>{this.state.greeting}, {this.props.user.firstName}!</h1>
        <button onClick={this.changeState}>change my state</button>
      </div>
    );
  }

  componentDidMount() {
    console.log('Hello: componentDidMount');
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps: ' + JSON.stringify(nextProps));
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('Hello: shouldComponentUpdate: ');
    console.log('  nextProps: ' + JSON.stringify(nextProps));
    console.log('  nextState: ' + JSON.stringify(nextState));
    let ret = true;
    console.log('  returning ' + ret);
    return ret;
  }

  componentWillUpdate(nextProps, nextState) {
    console.log('Hello: componentWillUpdate: ');
    console.log('  nextProps: ' + JSON.stringify(nextProps));
    console.log('  nextState: ' + JSON.stringify(nextState));
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('Hello: componentDidUpdate: ');
    console.log('  prevProps: ' + JSON.stringify(prevProps));
    console.log('  prevState: ' + JSON.stringify(prevState));
  }

  componentWillUnmount() {
    console.log('Hello: componentWillUnmount');
  }
}  
