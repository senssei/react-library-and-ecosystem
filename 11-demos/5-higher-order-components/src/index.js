import React from 'react';
import { render } from 'react-dom';

const WithState = Component =>
  class extends React.Component {
    constructor(props) {
      super(props);
      this.state = props.initialModel;
    }

    updateState = updateState => {
      this.setState(updateState);
    };

    render() {
      return React.createElement(Component, {
        ...this.props,
        ...this.state,
        ...{ updateState: this.updateState },
      });
    }
  };

const mapProps = fn => Component => props =>
  React.createFactory(Component)(fn(props));

const compose = (f, g) => x => f(g(x));

const Enhance = compose(
  mapProps(({ initialModel: { value, ...initialValues }, ...props }) => ({
    ...props,
    ...{ ...initialValues, ...{ id: value } },
  })),
  WithState,
);

const SomeStatelessFunction = ({ id, updateState, ...props }) => {
  return (
    <div>
      <div>
        Id: {id}
      </div>
      <button onClick={() => updateState({ id: id + 1 })}>Update</button>
    </div>
  );
};

const Enhanced = Enhance(SomeStatelessFunction);

const App = () => {
  return (
    <div>
      <Enhanced initialModel={{ value: 5 }} />
    </div>
  );
};

render(<App />, document.getElementById('root'));
