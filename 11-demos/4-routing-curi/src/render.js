import React from 'react';
import ReactDOM from 'react-dom';
import { CuriProvider } from '@curi/react';

import NavLinks from './components/NavLinks.js';

let root = document.getElementById('root');

export default ({ router }) => {
  ReactDOM.render((
    <CuriProvider router={router}>
      {({ response }) => {
        const { body: Body } = response;
        return (
          <div>
            <header>
              <NavLinks />
            </header>
            <main>
              <Body response={response} />
            </main>
          </div>
        );
        }}
    </CuriProvider>
  ), root);
};
