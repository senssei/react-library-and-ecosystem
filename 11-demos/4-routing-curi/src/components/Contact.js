import React from 'react';

const Contact = () => (
  <div className='contact'>
    You can contact us by fax at 1-206-555-0123.
  </div>
);

export default Contact;
