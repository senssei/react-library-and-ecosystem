import React from 'react';

const Home = () => (
  <div className='home'>
    Welcome to our book store!
  </div>
);

export default Home;
