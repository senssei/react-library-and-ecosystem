import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { forecast } from './data.js';
import { convertKtoC } from './temp.helper';



const App = () => {
  const { list } = forecast;

  const comp = list.map(e =>
    (
      <li key={e.dt}>
        <table>
          <thead>
            <tr>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{convertKtoC(e.main.temp) } C </td>
            </tr>
          </tbody>
        </table>
        {e.dt}
      </li>
    )
  );
  return (
    <div>
      <h1>Weather app</h1>
      <h2>{forecast.city.name}</h2>
      <ul>
        {list.length > 0 ? comp : null}
      </ul>

    </div>
  );
};


export default App;
