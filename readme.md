# React – the library and ecosystem

This training is heavily focuses on creating environment for hand-on experience rather than just present information.

All resources (code + presentations) will be delivered in repository for further refinement. 

## Format

Basic training will be 3 days with 8h a day schedule with 3-4 short breaks and 1 lunch break. Training can be stripped and condensed to 2 days with additional work done by participants.

Group can consist from 2-6 participant per single trainer to consider it feasible. 

## Requirements

* Basic knowledge of functional programming elements in JavaScript ( .map, .reduce )
* Solid experience with latest JavaScript ( es2015+ : imports, arrow functions, babel, spread operators etc )
*Standard laptop with internet and installed current or LTS node.js, git, Chrome + with or without plugins like: React Development Tools and Redux DevTools and code editor. All examples will be tested on free Visual Studio Code and available in Git repository.
* Additional time to review resources and exercises after training
 
## Agenda

1. Intro to React
    * glossary
    * what's VDOM (how React abstracts away the view layer to achieve cross-target rendering)
2. Setting up a project
    * webpack, babel (what purpose do those serve, without going in-deep about config)
    * CRA
    * IDE
    * browser tools (react devtools tips & tricks)
    * linting (using eslint, run down of the popular presets)
3. React basics
    * ReactDOM.render
    * JSX
    * your first component
    * SFC vs Component (discussing the differences, pros and cons) 
    * React.createElement (to help understand JSX and some of its limitations)
    * composition 
    * passing data down
    * passing data up
    * propTypes
    * working with list 
    * component state
    * lifecycle
    * event handling 
    * ref explained via document.getElementById example
    * controlled vs. uncontrolled forms
    * thinking in react - list the state up -> lift state
    * React recipes 
    * styling components , css-loader
    * styled-components
    * communicating with outside world (ajax) -> provide a simple json-server to mock the API, work with it
    * routing
5. Redux
    * short into
    * you might / might not need redux if
    * actions, action creators, reducers
    * middleware
    * react-redux
    * integrate redux into our app
    * create reducers, actions, action creators
    * selectors 
    * optimizing selectors with reselect
6. Testing React applications
    * what to test
    * testing using jest
    * snapshot testing
7. Production build and deployment

## Additional materials
* https://egghead.io/browse/frameworks/react
* https://egghead.io/browse/libraries/redux
* https://www.udemy.com/react-redux/learn/v4/
* https://facebook.github.io/react/docs/tutorial.html
* https://github.com/mikechau/react-primer-draft
* https://engineering.hexacta.com/didact-learning-how-react-works-by-building-it-from-scratch-51007984e5c5


