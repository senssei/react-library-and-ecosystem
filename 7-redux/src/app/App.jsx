import React from 'react';

import Users from '../user/Users';
import Posts from '../post/Posts';

export default function App() {
  return (
    <div>
      <Users />
      <Posts />
    </div>
  );
}
